return {
  "neovim/nvim-lspconfig",
  event = "BufReadPost",
  dependencies = {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "saghen/blink.cmp",
    "folke/lazydev.nvim",
    "j-hui/fidget.nvim",
    {
      "pmizio/typescript-tools.nvim",
      dependencies = { "nvim-lua/plenary.nvim" },
    },
  },
  config = function()
    local lsp = require "lspconfig"
    local typescript = require "typescript-tools"

    vim.api.nvim_create_autocmd("LspAttach", {
      group = vim.api.nvim_create_augroup("UserLspConfig", { clear = true }),
      callback = function(ev)
        vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, { buffer = ev.buf })
        vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, { buffer = ev.buf })
        vim.keymap.set("n", "<leader>s", vim.lsp.buf.signature_help, { buffer = ev.buf })

        vim.keymap.set("n", "gd", require("telescope.builtin").lsp_definitions, { buffer = ev.buf })
        vim.keymap.set("n", "gt", require("telescope.builtin").lsp_type_definitions, { buffer = ev.buf })
        vim.keymap.set("n", "gr", require("telescope.builtin").lsp_references, { buffer = ev.buf })

        -- See `:help K` for why this keymap
        vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = ev.buf })
      end,
    })

    local capabilities = require("blink.cmp").get_lsp_capabilities()

    -- local capabilities = vim.lsp.protocol.make_client_capabilities()
    -- capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())

    local handlers = {
      ["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "single" }),
      ["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = "single" }),
    }

    require("mason").setup()
    require("mason-lspconfig").setup {
      automatic_installation = true,
      ensure_installed = { "lua_ls", "ts_ls", "eslint" },
    }

    lsp.lua_ls.setup {
      capabilities = capabilities,
      handlers = handlers,
    }

    typescript.setup {
      capabilities = capabilities,
      handlers = handlers,
      settings = {
        expose_as_code_action = "all",
      },
    }

    lsp.eslint.setup {
      capabilities = capabilities,
    }

    lsp.jsonls.setup {
      capabilities = capabilities,
      settings = {
        json = {
          schemas = {
            {
              fileMatch = { "package.json" },
              url = "https://json.schemastore.org/package.json",
            },
            {
              fileMatch = { "tsconfig.json", "tsconfig.*.json" },
              url = "http://json.schemastore.org/tsconfig",
            },
          },
        },
      },
    }

    lsp.yamlls.setup {
      settings = {
        yaml = {
          schemas = {
            ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
          },
        },
      },
    }

    lsp.marksman.setup {
      capabilities = capabilities,
    }

    lsp.sqlls.setup {
      capabilities = capabilities,
    }

    lsp.bashls.setup {
      capabilities = capabilities,
    }

    lsp.jqls.setup {
      capabilities = capabilities,
    }
  end,
}
