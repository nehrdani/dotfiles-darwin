return {
  "catppuccin/nvim",
  name = "catppuccin",
  lazy = false,
  priority = 1000,
  opts = {
    styles = {
      comments = { "italic" },
      conditionals = {},
      keywords = { "italic" },
      types = { "bold" },
    },
    integrations = {
      fidget = true,
      native_lsp = {
        enabled = true,
        underlines = {
          errors = { "undercurl" },
          hints = { "undercurl" },
          warnings = { "undercurl" },
          information = { "undercurl" },
        },
      },
    },
  },
  init = function()
    vim.cmd.colorscheme "catppuccin"
  end,
}
