return {
  "j-hui/fidget.nvim",
  opts = {
    progress = {
      display = {
        overrides = {
          ["typescript-tools"] = { name = "typescript" },
        },
      },
    },
    notification = {
      window = { winblend = 0 },
    },
  },
}
