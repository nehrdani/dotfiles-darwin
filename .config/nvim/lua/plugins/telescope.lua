return {
  "nvim-telescope/telescope.nvim",
  branch = "0.1.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },
  },
  cmd = "Telescope",
  config = function()
    local telescope = require "telescope"
    local actions = require "telescope.actions"

    telescope.setup {
      defaults = {
        wrap_results = true,
        mappings = {
          n = {
            ["q"] = actions.close,
            ["/"] = function()
              vim.cmd "startinsert"
            end,
          },
        },
      },
      pickers = {
        find_files = {
          theme = "dropdown",
          find_command = { "fd", "--type", "f", "--hidden", "--exclude", ".git" },
        },
        buffers = { theme = "dropdown", sort_lastused = true },
        current_buffer_fuzzy_find = { theme = "dropdown", previewer = false },
        diagnostics = { line_width = 100 },
      },
    }

    pcall(telescope.load_extension, "fzf")
  end,
  keys = function()
    local builtin = require "telescope.builtin"

    return {
      {
        "<leader>fh",
        function()
          builtin.help_tags()
        end,
        desc = "[F]ind [H]elp",
      },
      {
        "<leader>fk",
        function()
          builtin.keymaps()
        end,
        desc = "[F]ind [K]eymaps",
      },
      {
        "<leader>ff",
        function()
          builtin.find_files()
        end,
        desc = "[F]ind [F]iles",
      },
      {
        "<leader>fs",
        function()
          builtin.builtin()
        end,
        desc = "[F]ind [S]elect Telescope",
      },
      {
        "<leader>fw",
        function()
          builtin.grep_string()
        end,
        desc = "[F]ind current [W]ord",
      },
      {
        "<leader>fg",
        function()
          builtin.live_grep()
        end,
        desc = "[F]ind by [G]rep",
      },
      {
        "<leader>fd",
        function()
          builtin.diagnostics()
        end,
        desc = "[F]ind [D]iagnostics",
      },
      {
        "<leader>fr",
        function()
          builtin.resume()
        end,
        desc = "[F]ind [R]esume",
      },
      {
        "<leader>f.",
        function()
          builtin.oldfiles()
        end,
        desc = '[F]ind Recent Files ("." for repeat)',
      },
      {
        "<leader><leader>",
        function()
          builtin.buffers()
        end,
        desc = "[ ] Find existing Buffers",
      },
      {
        "<leader>/",
        function()
          builtin.current_buffer_fuzzy_find()
        end,
        desc = "[/] Fuzzily search in current buffer]",
      },
      {
        "<C-p>",
        function()
          builtin.git_files()
        end,
        desc = "[C-p] Find git files",
      },
      {
        "<leader>fn",
        function()
          builtin.find_files { cwd = vim.fn.stdpath "config" }
        end,
        desc = "[F]ind [N]eovim files",
      },
    }
  end,
}
