return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
  event = "BufReadPost",
  cmd = {
    "TSUpdate",
    "TSInstallInfo",
    "TSEnable",
    "TSDisable",
    "TSModuleInfo",
    "TSUninstall",
  },
  config = function()
    require("nvim-treesitter.configs").setup {
      ensure_installed = {
        "lua",
        "javascript",
        "typescript",
        "tsx",
        "json",
        "jsonc",
        "yaml",
        "css",
        "scss",
        "html",
        "markdown",
        "markdown_inline",
        "comment",
        "regex",
        "sql",
        "bash",
        "xml",
        "jq",
      },
      sync_install = false,
      auto_install = false,
      highlight = { enable = true },
      indent = { enable = true },
      incremental_selection = {
        enable = true,
        keymaps = {
          node_incremental = "v",
          node_decremental = "V",
        },
      },
    }
  end,
}
