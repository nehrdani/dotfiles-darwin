return {
  'stevearc/oil.nvim',
  dependencies = { 'nvim-tree/nvim-web-devicons' },
  cmd = 'Oil',
  opts = {
    default_file_explorer = false,
    view_options = {
      show_hidden = true,
    },
  },
  keys = function()
    local oil = require('oil')
    return {
      {
        '<leader>-',
        function() oil.toggle_float() end,
        desc = 'Browse Files',
      },
    }
  end
}
