return {
  "stevearc/dressing.nvim",
  event = "VeryLazy",
  opts = {
    select = {
      builtin = {
        mappings = {
          ["q"] = "Close",
          ["<C-y>"] = "Confirm",
        },
      },
      get_config = function(opts)
        if opts.kind == "codeaction" then
          return {
            backend = "builtin",
            builtin = {
              relative = "cursor",
              max_width = 40,
              min_height = 1,
            },
          }
        end
      end,
    },
  },
}
