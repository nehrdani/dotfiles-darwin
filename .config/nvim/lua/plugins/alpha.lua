return {
  "goolord/alpha-nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  event = "VimEnter",
  opts = function()
    local theme = require "alpha.themes.theta"
    local dashboard = require "alpha.themes.dashboard"

    local logo = {
      type = "text",
      val = {
        [[88888b.   .d88b.   .d88b.  888  888 888 88888b.d88b. ]],
        [[888 '88b d8P  Y8b d88''88b 888  888 888 888 '888 '88b]],
        [[888  888 88888888 888  888 Y88  88P 888 888  888  888]],
        [[888  888 Y8b.     Y88..88P  Y8bd8P  888 888  888  888]],
        [[888  888  'Y8888   'Y88P'    Y88P   888 888  888  888]],
      },
      opts = {
        hl = "AlphaHeader",
        -- hl = 'Type',
        position = "center",
      },
    }

    -- MRU setup
    local mru_val = {
      { type = "text", val = "Recent Files", opts = { hl = "AlphaButtons", position = "center" } },
      { type = "padding", val = 1 },
    }
    local mru_list = theme.mru(1, vim.fn.getcwd(), 8)
    for _, file in ipairs(mru_list.val) do
      table.insert(mru_val, file)
    end
    local mru = {
      type = "group",
      val = mru_val,
    }

    -- Actions setup
    local actions = {
      type = "group",
      val = {
        { type = "text", val = "Quick actions", opts = { hl = "AlphaButtons", position = "center" } },
        { type = "padding", val = 1 },
        dashboard.button("e", "󰝒  New file", "<cmd>ene<CR>"),
        dashboard.button("f", "󰱼  Find file", "<cmd>Telescope find_files<CR>"),
        dashboard.button("g", "󰊄  Live grep", "<cmd>Telescope live_grep<CR>"),
        dashboard.button("b", "󰉋  Browse files", "<cmd>Oil<CR>"),
        dashboard.button(".", "󰈢  Recent files", "<cmd>Telescope oldfiles<CR>"),
        { type = "padding", val = 1 },
        dashboard.button("c", "  Configuration", "<cmd>e ~/.config/nvim/init.lua <CR>"),
        dashboard.button("m", "󱁤  Mason", "<cmd>Mason<CR>"),
        dashboard.button("l", "󰒲  Lazy", "<cmd>Lazy<CR>"),
        dashboard.button("t", "󰭽  Update TS parsers", "<cmd>TSUpdate<CR>"),
        { type = "padding", val = 1 },
        dashboard.button("q", "󰍃  Quit", "<cmd>qa<CR>"),
      },
      position = "center",
    }

    -- Info setup
    local version = vim.version()
    local info = {
      type = "text",
      val = " v" .. version.major .. "." .. version.minor .. "." .. version.patch,
      opts = {
        -- hl = 'AlphaFooter',
        hl = "Type",
        position = "center",
      },
    }

    theme.config.layout = {
      { type = "padding", val = 1 },
      logo,
      { type = "padding", val = 2 },
      mru,
      { type = "padding", val = 2 },
      actions,
      { type = "padding", val = 1 },
      info,
    }

    return theme
  end,
  config = function(_, theme)
    -- close Lazy and re-open when the dashboard is ready
    if vim.o.filetype == "lazy" then
      vim.cmd.close()
      vim.api.nvim_create_autocmd("User", {
        pattern = "AlphaReady",
        callback = function()
          require("lazy").show()
        end,
      })
    end

    require("alpha").setup(theme.config)

    vim.api.nvim_create_autocmd("User", {
      callback = function()
        local stats = require("lazy").stats()
        local ms = math.floor(stats.startuptime * 100) / 100
        theme.config.layout[9] = {
          type = "text",
          val = "󱐌 Lazy loaded " .. stats.loaded .. " plugins in " .. ms .. "ms",
          opts = {
            -- hl = 'AlphaFooter',
            hl = "Type",
            position = "center",
          },
        }
        pcall(vim.cmd.AlphaRedraw)
      end,
    })
  end,
}
