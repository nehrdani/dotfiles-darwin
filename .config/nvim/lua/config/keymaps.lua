-- Set leader key to space
vim.g.mapleader = " "
vim.g.maplocalleader = " "

local set = vim.keymap.set

-- Ergonomic normal mode
set("i", "jk", "<Esc>")

-- Delete without yanking
set("n", "x", [["_x]])
set("n", "<leader>d", [["_d]])

-- Replace without yanking
set("x", "<leader>p", [["_dP]])

-- Copy to clipboard
set({ "n", "v" }, "<leader>y", [["+y]])
set("n", "<leader>Y", [["+Y]])

-- Move lines up or down
set("v", "J", ":m '>+1<CR>gv=gv")
set("v", "K", ":m '<-2<CR>gv=gv")

-- Increment/decrement
set("n", "+", "<C-a>")
set("n", "-", "<C-x>")

-- Clear highlighting
set("n", "<C-c>", "<cmd>noh<CR>", { nowait = true, silent = true })
set("n", "<Esc>", "<cmd>noh<CR>", { nowait = true, silent = true })

-- Split window
set("n", "<C-w>s", ":split <CR>")
set("n", "<C-w>v", ":vsplit <CR>")

-- Move window
set("n", "<C-h>", "<C-w>h")
set("n", "<C-k>", "<C-w>k")
set("n", "<C-j>", "<C-w>j")
set("n", "<C-l>", "<C-w>l")

-- Resize window
set("n", "<C-w><left>", "<C-w>4<")
set("n", "<C-w><right>", "<C-w>4>")
set("n", "<C-w><up>", "<C-w>4+")
set("n", "<C-w><down>", "<C-w>4-")

-- Quickfix list
set("n", "<leader>co", ":copen<CR>")
set("n", "<leader>cc", ":cclose<CR>")
set("n", "[q", ":cprev<CR>")
set("n", "]q", ":cnext<CR>")

-- Diagnostic
set("n", "[d", vim.diagnostic.goto_next)
set("n", "]d", vim.diagnostic.goto_prev)
set("n", "<leader>e", vim.diagnostic.open_float)
