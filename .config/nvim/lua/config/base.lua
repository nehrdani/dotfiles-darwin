local set = vim.opt

set.number = true
set.relativenumber = true
set.signcolumn = "yes"
set.ruler = false
set.showmode = false
set.backup = false
set.swapfile = false
set.undofile = true
set.tabstop = 2
set.shiftwidth = 2
set.expandtab = true
set.smartindent = true
set.autoindent = true
set.breakindent = true
set.ignorecase = true
set.smartcase = true
set.scrolloff = 8
set.splitright = true
set.splitbelow = true
set.wrap = false
set.list = true
set.listchars = { tab = "» ", trail = "·", nbsp = "␣" }
set.path:append { "**" } -- Finding files - Search down into subfolders
set.wildignore:append { "*/node_modules/*" }
set.shell = "zsh"
-- set.clipboard:append { 'unnamedplus' }
set.cursorline = true
set.termguicolors = true
set.fillchars:append { diff = "/" }

set.shortmess:append "C"
set.shortmess:append "S" -- We have our own search counter
set.shortmess:append "c"
set.shortmess:append "s"

set.updatetime = 250
set.timeoutlen = 300

-- detect .env.local or .env.example as env files
vim.filetype.add {
  pattern = {
    [".env.*"] = "sh",
  },
}

vim.diagnostic.config {
  virtual_text = false,
  underline = true,
  update_in_insert = true,
  severity_sort = true,
  float = { border = "single", source = "if_many", scope = "cursor" },
  signs = {
    text = {
      [vim.diagnostic.severity.ERROR] = "󰅚 ",
      [vim.diagnostic.severity.WARN] = "󰀪 ",
      [vim.diagnostic.severity.HINT] = "󰌶 ",
      [vim.diagnostic.severity.INFO] = "󰋽 ",
    },
  },
}
