#!/usr/bin/env bash

scratchpad_space=${2:-8}
app_id=$(yabai -m query --windows | jq ".[] | select(.app==\"$1\").id")
app_space=$(yabai -m query --windows --window "$app_id" | jq '.space')
is_floating=$(yabai -m query --windows --window "$app_id" | jq '.["is-floating"]')
current_space=$(yabai -m query --spaces --space | jq '.index')

if [ -z "$app_id" ]; then
  exit 0
fi

if [[ "$is_floating" == "false" ]]; then
  yabai -m window "$app_id" --toggle float
  yabai -m window "$app_id" --grid 4:4:1:1:2:2
fi

if [ "$current_space" -eq "$app_space" ]; then
  yabai -m window "$app_id" --space "$scratchpad_space" 
else
  yabai -m window "$app_id" --space "$current_space" 
  yabai -m window "$app_id" --focus
fi
