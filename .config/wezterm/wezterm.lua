local wezterm = require('wezterm')
local config = wezterm.config_builder()

-- Disable bell
config.audible_bell = 'Disabled'

-- Font
config.font = wezterm.font('JetBrains Mono')
config.font_size = 12
config.line_height = 1

-- Theming
config.color_scheme = 'Catppuccin Mocha'

-- Window
config.window_decorations = "RESIZE|MACOS_FORCE_DISABLE_SHADOW"
config.window_padding = {
  left = 0,
  right = 0,
  top = '0.5cell',
  bottom = 0,
}
config.adjust_window_size_when_changing_font_size = false
config.window_close_confirmation = 'NeverPrompt'

-- Tabs
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true
config.show_new_tab_button_in_tab_bar = false
config.show_new_tab_button_in_tab_bar = false
config.hide_tab_bar_if_only_one_tab = true
config.enable_tab_bar = false
config.switch_to_last_active_tab_when_closing_tab = true

-- Compose key
config.send_composed_key_when_left_alt_is_pressed = true
config.send_composed_key_when_right_alt_is_pressed = false

-- Keybindings
config.leader = { key = 'w', mods = 'CTRL' }
config.keys = {
  {
    key = '|',
    mods = 'LEADER',
    action = wezterm.action.SplitHorizontal,
  },
  {
    key = '-',
    mods = 'LEADER',
    action = wezterm.action.SplitVertical,
  },
  {
    key = 'x',
    mods = 'LEADER',
    action = wezterm.action.CloseCurrentPane({ confirm = false }),
  },
  {
    key = 'c',
    mods = 'LEADER',
    action = wezterm.action.SpawnTab('CurrentPaneDomain'),
  },
  {
    key = '&',
    mods = 'LEADER',
    action = wezterm.action.CloseCurrentTab({ confirm = false }),
  },
  {
    key = ',',
    mods = 'LEADER',
    action = wezterm.action.PromptInputLine({
      description = 'Enter new name for tab',
      action = wezterm.action_callback(
        function(window, _, line)
          if line then
            window:active_tab():set_title(line)
          end
        end
      )
    }),
  },
  {
    key = 'n',
    mods = 'LEADER',
    action = wezterm.action.ActivateTabRelative(1),
  },
  {
    key = 'p',
    mods = 'LEADER',
    action = wezterm.action.ActivateTabRelative(-1),
  },
  {
    key = 'n',
    mods = 'LEADER|SHIFT',
    action = wezterm.action.MoveTabRelative(1),
  },
  {
    key = 'p',
    mods = 'LEADER|SHIFT',
    action = wezterm.action.MoveTabRelative(-1),
  },
  {
    key = 'w',
    mods = 'LEADER',
    action = wezterm.action.ShowLauncherArgs({ flags = 'FUZZY|TABS' }),
  },
  {
    key = 'j',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection('Down'),
  },
  {
    key = 'k',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection('Up'),
  },
  {
    key = 'h',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection('Left'),
  },
  {
    key = 'l',
    mods = 'LEADER',
    action = wezterm.action.ActivatePaneDirection('Right'),
  },
  {
    key = '{',
    mods = 'LEADER',
    action = wezterm.action.PaneSelect({ mode = 'SwapWithActiveKeepFocus' }),
  },
  {
    key = '[',
    mods = 'LEADER',
    action = wezterm.action.ActivateCopyMode,
  },
  {
    key = 's',
    mods = 'LEADER',
    action = wezterm.action.ShowLauncherArgs({ flags = 'FUZZY|WORKSPACES' }),
  },
  {
    key = 's',
    mods = 'LEADER|SHIFT',
    action = wezterm.action.PromptInputLine({
      description = 'Enter name for workspace',
      action = wezterm.action_callback(
        function(window, pane, line)
          if line then
            window:perform_action(
              wezterm.action.SwitchToWorkspace {
                name = line,
              },
              pane
            )
          end
        end
      )
    }),
  },
  {
    key = '$',
    mods = 'LEADER',
    action = wezterm.action.PromptInputLine({
      description = 'Enter new name for workspace',
      action = wezterm.action_callback(
        function(window, _, line)
          if line then
            wezterm.mux.rename_workspace(
              window:mux_window():get_workspace(),
              line
            )
          end
        end
      )
    }),
  },
  {
    key = 'n',
    mods = 'CTRL|SHIFT',
    action = wezterm.action.DisableDefaultAssignment,
  },
  {
    key = 'p',
    mods = 'CTRL|SHIFT',
    action = wezterm.action.DisableDefaultAssignment,
  },
}

return config
