export EDITOR='nvim'
export VISUAL='nvim'
export TERM='wezterm'
# gpg
export GPG_TTY=$(tty)

export PATH=$PATH:$HOME/scripts

# Android Studio
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools

### Alias
alias vim='nvim'
alias v='nvim'
alias ls='eza'
alias l='eza -al --color=always --group-directories-first'

alias vimconf='nvim ~/.config/nvim/init.lua'
alias zshconf='nvim ~/.zshrc'

# bare git repo alias for dotfiles
alias dot="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

# Useful functions
dotenv() {
  export $(cat ${1:-.env} | xargs)
}


### FZF
FZF_THEME="--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8,fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc,marker:#b4befe,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8,selected-bg:#45475a"
export FZF_DEFAULT_OPTS="$FZF_THEME --bind=ctrl-y:accept"
export FZF_CTRL_T_OPTS="--height 50% --tmux 80% --border --preview 'bat --color=always -n --line-range :500 {}'"
export FZF_CTRL_R_OPTS="--height 50% --tmux 80% --border"

source <(fzf --zsh)


### ZSH
ZSH_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zsh"

# Enable vi mode
bindkey -v
export KEYTIMEOUT=1

# History options
HISTSIZE=2000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase

setopt appendhistory
setopt sharehistory
setopt extendedhistory
setopt hist_ignore_space
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_find_no_dups

# Completions
if type brew &>/dev/null
then
  FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"
fi

zmodload zsh/complist
autoload -Uz compinit; compinit

# FZF Tab completions
if [ ! -f "$ZSH_HOME/fzf-tab/fzf-tab.plugin.zsh" ]; then
  git clone --depth 1 https://github.com/Aloxaf/fzf-tab.git "$ZSH_HOME/fzf-tab"
fi
source "$ZSH_HOME/fzf-tab/fzf-tab.plugin.zsh"

zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' menu no
zstyle ':fzf-tab:*' fzf-flags $FZF_THEME --bind=ctrl-y:accept

# Edit prompt in $EDITOR
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

bindkey -M viins '^?' backward-delete-char # [Backspace] - delete backward (vi mode fix)

bindkey -M vicmd '^p' history-search-backward
bindkey -M viins '^p' history-search-backward

bindkey -M vicmd '^n' history-search-forward
bindkey -M viins '^n' history-search-forward

# Autosuggest
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
ZSH_AUTOSUGGEST_MANUAL_REBIND=true

if [ ! -f "$ZSH_HOME/zsh-autosuggestions/zsh-autosuggestions.zsh" ]; then
  git clone --depth 1 https://github.com/zsh-users/zsh-autosuggestions.git "$ZSH_HOME/zsh-autosuggestions"
fi
source "$ZSH_HOME/zsh-autosuggestions/zsh-autosuggestions.zsh"

bindkey -M viins '^y' autosuggest-accept
bindkey -M vicmd '^y' autosuggest-accept

bindkey -M viins '^l' forward-word
bindkey -M vicmd '^l' forward-word

bindkey -M viins '^h' backward-delete-word
bindkey -M vicmd '^h' backward-delete-word

# Highlighting
if [ ! -f "$ZSH_HOME/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]; then
  git clone --depth 1 https://github.com/zsh-users/zsh-syntax-highlighting.git "$ZSH_HOME/zsh-syntax-highlighting"
fi
source "$ZSH_HOME/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"


# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion


# fnm (Better nvm)
eval "$(fnm env --use-on-cd --version-file-strategy=recursive)"

eval "$(zoxide init zsh)"
eval "$(starship init zsh)"
